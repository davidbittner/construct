use anyhow::Result;
use gltf::Gltf;
use log::*;
use std::collections::HashMap;
use std::io::Read;
use std::path::PathBuf;
use std::sync::Arc;

type GlDictionary = HashMap<GlResourceType, HashMap<String, usize>>;

#[derive(Debug)]
pub enum Asset {
    Model(Gltf),
    Text(String),
    Blob(Vec<u8>),
}

#[derive(Debug)]
pub enum GlResourceType {
    Mesh,
    Texture,
    Shader,
}

#[derive(Debug, Default)]
pub struct AssetDictionary {
    gl_dict: GlDictionary,
    asset_dict: HashMap<String, Arc<Asset>>,
}

impl AssetDictionary {
    pub fn load(&mut self, path: String) -> Result<()> {
        let pth = PathBuf::from(&path);
        let ext = pth
            .extension()
            .map(|s| s.to_string_lossy().to_string())
            .unwrap_or_default();

        let loaded = match ext.as_str() {
            "gltf" => Asset::Model(Gltf::open(&path)?),
            "txt" => {
                let mut file = std::fs::File::open(&path)?;
                let mut contents = String::new();
                file.read_to_string(&mut contents)?;

                Asset::Text(contents)
            }
            "" => {
                let mut buff = Vec::new();
                let mut file = std::fs::File::open(&path)?;
                file.read_to_end(&mut buff)?;

                Asset::Blob(buff)
            }
            _ => anyhow::bail!("unknown extension: '{}'", ext),
        };

        if self.asset_dict.contains_key(&path) {
            anyhow::bail!("attempted to load asset: '{}' but there is already an asset loaded under this path", path)
        } else {
            self.asset_dict.insert(path, Arc::new(loaded));
            Ok(())
        }
    }

    pub fn get_asset(&mut self, path: String) -> Result<Arc<Asset>> {
        if let Some(asset) = self.asset_dict.get(&path) {
            Ok(Arc::clone(asset))
        } else {
            anyhow::bail!("asset '{}' does not exist", path)
        }
    }

    pub fn remove_asset(&mut self, path: String) {
        let res = self.asset_dict.remove(&path);
        if res.is_none() {
            log::warn!(
                "tried to remove asset, but one at the path wasn't loaded: '{}'",
                path
            );
        } else {
            log::info!("successfully deleted asset '{}'", path);
        }
    }

    pub fn gl_init(&mut self, path: String, asset_type: GlResourceType) -> Result<usize> {
        let asset = self
            .asset_dict
            .get(&path)
            .ok_or(anyhow::format_err!("asset '{}' is not loaded", path))?;

        Ok(5)
    }
}
