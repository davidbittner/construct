use legion::*;

mod asset_dictionary;
mod camera;
mod components;
mod game_loop;
mod keyboard;
mod vulkan;

use vulkan::vulk_main;

use components::*;
use std::time::{Duration, Instant};
use vulkano::buffer::{BufferUsage, CpuAccessibleBuffer};
use vulkano::command_buffer::{AutoCommandBufferBuilder, DynamicState, SubpassContents};

use vulkano::framebuffer::Subpass;

use vulkano::swapchain;
use vulkano::swapchain::{AcquireError, SwapchainCreationError};
use vulkano::sync;
use vulkano::sync::{FlushError, GpuFuture};

use std::sync::Arc;
use vulkano::pipeline::GraphicsPipeline;

use winit::event::{Event, WindowEvent};
use winit::event_loop::ControlFlow;

const WINDOW_SIZE: (u32, u32) = (800, 600);

pub struct Time {
    start: Instant,
}

impl std::default::Default for Time {
    fn default() -> Self {
        Time {
            start: Instant::now(),
        }
    }
}

impl Time {
    pub fn start_frame(&mut self) {
        self.start = Instant::now();
    }

    pub fn delta_time(&self) -> Duration {
        let now = Instant::now();
        now - self.start
    }
}

#[system(for_each)]
fn proc_velocities(pos: &mut Position, vel: &Velocity, #[resource] time: &Time) {
    let dt = time.delta_time().as_secs_f32();
    pos.x += vel.x * dt;
    pos.y += vel.y * dt;
    pos.z += vel.z * dt;
}

fn main() {
    pretty_env_logger::try_init_timed_custom_env("CONSTRUCT").unwrap();

    let mut world = World::default();
    let _: Entity = world.push((
        Position {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        },
        Velocity {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        },
        Render {
            color: (0.5, 0.5, 0.5),
        },
    ));

    let mut resources = Resources::default();
    let mut schedule = Schedule::builder()
        .add_system(proc_velocities_system())
        .build();

    resources.insert(Time::default());

    let mut game = game_loop::Game::new(world, resources, schedule);
    let (surface, el, device, mut swapchain, images, queue) = vulk_main();

    #[derive(Default, Debug, Clone)]
    struct Vertex {
        position: [f32; 2],
    }
    vulkano::impl_vertex!(Vertex, position);
    let vertex_buffer = {
        CpuAccessibleBuffer::from_iter(
            device.clone(),
            BufferUsage::all(),
            false,
            [
                Vertex {
                    position: [-0.5, -0.25],
                },
                Vertex {
                    position: [0.0, 0.5],
                },
                Vertex {
                    position: [0.25, -0.1],
                },
            ]
            .iter()
            .cloned(),
        )
        .unwrap()
    };

    mod vs {
        vulkano_shaders::shader! {
            ty: "vertex",
            src: "
                #version 450
                #extension GL_ARB_separate_shader_objects : enable
                layout(location = 0) in vec2 position;
                layout(location = 0) out vec3 v_color;

                vec3 colors[3] = vec3[](
                    vec3(1.0, 0.0, 0.0),
                    vec3(0.0, 1.0, 0.0),
                    vec3(0.0, 0.0, 1.0)
                );

                void main() {
                    gl_Position = vec4(position, 0.0, 1.0);
                    v_color = colors[gl_VertexIndex];
                }
            "
        }
    }

    mod fs {
        vulkano_shaders::shader! {
            ty: "fragment",
            src: "
                #version 450
                #extension GL_ARB_separate_shader_objects : enable
                layout(location = 0) out vec4 f_color;
                layout(location = 0) in vec3 v_color;

                void main() {
                    f_color = vec4(v_color, 1.0);
                }
            "
        }
    }

    let vs = vs::Shader::load(device.clone()).unwrap();
    let fs = fs::Shader::load(device.clone()).unwrap();

    let render_pass = Arc::new(
        vulkano::single_pass_renderpass!(
            device.clone(),
            attachments: {
                color: {
                    load: Clear,
                    store: Store,
                    format: swapchain.format(),
                    samples: 1,
                }
            },
            pass: {
                color: [color],
                depth_stencil: {}
            }
        )
        .unwrap(),
    );

    let pipeline = Arc::new(
        GraphicsPipeline::start()
            .vertex_input_single_buffer::<Vertex>()
            .vertex_shader(vs.main_entry_point(), ())
            .triangle_list()
            .viewports_dynamic_scissors_irrelevant(1)
            .fragment_shader(fs.main_entry_point(), ())
            .render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
            .build(device.clone())
            .unwrap(),
    );

    let mut dynamic_state = DynamicState {
        line_width: None,
        viewports: None,
        scissors: None,
        compare_mask: None,
        write_mask: None,
        reference: None,
    };

    let mut framebuffers =
        vulkan::window_size_dependent_setup(&images, render_pass.clone(), &mut dynamic_state);

    let mut recreate_swapchain = false;
    let mut previous_frame_end = Some(sync::now(device.clone()).boxed());
    el.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Poll;
        match event {
            Event::LoopDestroyed => return,
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::Resized(_) => {
                    recreate_swapchain = true;
                }
                WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
                _ => (),
            },
            Event::RedrawEventsCleared => {
                previous_frame_end.as_mut().unwrap().cleanup_finished();
                if recreate_swapchain {
                    let dims: [u32; 2] = surface.window().inner_size().into();
                    let (new_swapchain, new_images) = match swapchain.recreate_with_dimensions(dims)
                    {
                        Ok(r) => r,
                        Err(SwapchainCreationError::UnsupportedDimensions) => return,
                        Err(e) => panic!("Failed to recreate swapchain: '{}'", e),
                    };

                    swapchain = new_swapchain;
                    framebuffers = vulkan::window_size_dependent_setup(
                        &new_images,
                        render_pass.clone(),
                        &mut dynamic_state,
                    );
                    recreate_swapchain = false;
                }

                let (image_num, suboptimal, acquire_future) =
                    match swapchain::acquire_next_image(swapchain.clone(), None) {
                        Ok(r) => r,
                        Err(AcquireError::OutOfDate) => {
                            recreate_swapchain = true;
                            return;
                        }
                        Err(e) => panic!("Failed to acquire next image: '{}'", e),
                    };

                if suboptimal {
                    recreate_swapchain = true;
                }

                let clear_values = vec![[0.2, 0.2, 0.2, 1.0].into()];
                let mut builder = AutoCommandBufferBuilder::primary_one_time_submit(
                    device.clone(),
                    queue.family(),
                )
                .unwrap();

                builder
                    .begin_render_pass(
                        framebuffers[image_num].clone(),
                        SubpassContents::Inline,
                        clear_values,
                    )
                    .unwrap()
                    .draw(
                        pipeline.clone(),
                        &dynamic_state,
                        vertex_buffer.clone(),
                        (),
                        (),
                    )
                    .unwrap()
                    .end_render_pass()
                    .unwrap();

                let command_buffer = builder.build().unwrap();
                let future = previous_frame_end
                    .take()
                    .unwrap()
                    .join(acquire_future)
                    .then_execute(queue.clone(), command_buffer)
                    .unwrap()
                    .then_swapchain_present(queue.clone(), swapchain.clone(), image_num)
                    .then_signal_fence_and_flush();

                match future {
                    Ok(future) => {
                        previous_frame_end = Some(future.boxed());
                    }
                    Err(FlushError::OutOfDate) => {
                        recreate_swapchain = true;
                        previous_frame_end = Some(sync::now(device.clone()).boxed());
                    }
                    Err(e) => {
                        eprintln!("Failed to flush future: {:?}", e);
                    }
                }
            }
            _ => (),
        }

        game.poll();
        game.proc();
        game.render();
    });
}
