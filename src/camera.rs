use nalgebra::{Matrix4, RowVector4, Vector3};

pub struct Camera {
    pos: Vector3<f32>,
}

impl Camera {
    pub fn new() -> Self {
        Self {
            pos: Vector3::new(0.0, 0.0, 1.0),
        }
    }

    pub fn look_at(&mut self, oth_pos: Vector3<f32>) -> Matrix4<f32> {
        let dir = self.pos - oth_pos;
        let u_up = Vector3::new(0.0, 1.0, 0.0);
        let right = u_up.cross(&dir).normalize();
        let up = dir.cross(&right);

        let a_mat_rows = [
            RowVector4::new(right.x, right.y, right.z, 0.0),
            RowVector4::new(up.x, up.y, up.z, 0.0),
            RowVector4::new(dir.x, dir.y, dir.z, 0.0),
            RowVector4::new(0.0, 0.0, 0.0, 1.0),
        ];

        let b_mat_rows = [
            RowVector4::new(1.0, 0.0, 0.0, -self.pos.x),
            RowVector4::new(0.0, 1.0, 0.0, -self.pos.y),
            RowVector4::new(0.0, 0.0, 1.0, -self.pos.z),
            RowVector4::new(0.0, 0.0, 0.0, 1.0),
        ];

        let a_mat = Matrix4::from_rows(&a_mat_rows);
        let b_mat = Matrix4::from_rows(&b_mat_rows);

        a_mat * b_mat
    }
}
