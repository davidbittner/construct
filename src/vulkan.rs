use vulkano::image::{ImageUsage, SwapchainImage};
use vulkano::instance::{Instance, PhysicalDevice};
use vulkano::pipeline::viewport::Viewport;
use vulkano::swapchain;
use vulkano::swapchain::{
    ColorSpace, FullscreenExclusive, PresentMode, SurfaceTransform, Swapchain,
};
use vulkano::{
    framebuffer::{Framebuffer, FramebufferAbstract, RenderPassAbstract},
    instance::InstanceExtensions,
};

use vulkano::command_buffer::DynamicState;
use vulkano::{
    app_info_from_cargo_toml,
    device::{Device, DeviceExtensions, Queue},
};

use vulkano_win::VkSurfaceBuild;
use winit::event_loop::EventLoop;
use winit::window::{Window, WindowBuilder};

use std::sync::Arc;
pub(in crate) fn vulk_main() -> (
    Arc<swapchain::Surface<Window>>,
    EventLoop<()>,
    Arc<Device>,
    Arc<Swapchain<Window>>,
    Vec<Arc<SwapchainImage<Window>>>,
    Arc<Queue>,
) {
    let mut required_extensions = vulkano_win::required_extensions();
    required_extensions.ext_debug_utils = true;

    println!("Available Vulkan debug layers:");
    let wanted_layers: Vec<_> = vec![
        "VK_LAYER_KHRONOS_validation",
        //"VK_LAYER_RENDERDOC_Capture",
        //"VK_LAYER_LUNARG_api_dump",
        //"VK_LAYER_LUNARG_device_simulation",
        "VK_LAYER_LUNARG_monitor",
        "VK_LAYER_LUNARG_screenshot",
    ];
    let avail_layers = vulkano::instance::layers_list().unwrap();
    let layers: Vec<_> = avail_layers
        .filter(|el| {
            println!("\t{}", el.name());
            wanted_layers.contains(&el.name())
        })
        .map(|el| {
            let name = el.name();
            wanted_layers
                .iter()
                .find(|layer_name| *layer_name == &name)
                .cloned()
                .unwrap()
        })
        .collect();

    println!("\nUsing these debug layers:");
    layers.iter().for_each(|el| println!("\t{}", el));

    let app_info = app_info_from_cargo_toml!();
    let instance = Instance::new(Some(&app_info), &required_extensions, layers).unwrap();

    let phys = PhysicalDevice::enumerate(&instance)
        .next()
        .expect("failed to get physical device");

    println!("Using device: {} (type: {:?})", phys.name(), phys.ty());

    let event_loop = EventLoop::new();
    let surface = WindowBuilder::new()
        .build_vk_surface(&event_loop, instance.clone())
        .unwrap();

    let queue_family = phys
        .queue_families()
        .find(|&q| q.supports_graphics() && surface.is_supported(q).unwrap_or(false))
        .unwrap();

    let device_ext = DeviceExtensions {
        khr_swapchain: true,
        ..DeviceExtensions::none()
    };

    let (device, mut queues) = Device::new(
        phys,
        phys.supported_features(),
        &device_ext,
        [(queue_family, 0.5)].iter().cloned(),
    )
    .unwrap();

    let queue = queues.next().unwrap();
    let (swapchain, images) = {
        let caps = surface.capabilities(phys).unwrap();
        let alpha = caps.supported_composite_alpha.iter().next().unwrap();
        let format = caps.supported_formats[0].0;
        let dimensions: [u32; 2] = surface.window().inner_size().into();

        Swapchain::new(
            device.clone(),
            surface.clone(),
            caps.min_image_count,
            format,
            dimensions,
            1,
            ImageUsage::color_attachment(),
            &queue,
            SurfaceTransform::Identity,
            alpha,
            PresentMode::Fifo,
            FullscreenExclusive::Default,
            true,
            ColorSpace::SrgbNonLinear,
        )
        .unwrap()
    };

    (surface, event_loop, device, swapchain, images, queue)
}

pub(in crate) fn window_size_dependent_setup(
    images: &[Arc<SwapchainImage<Window>>],
    render_pass: Arc<dyn RenderPassAbstract + Send + Sync>,
    dynamic_state: &mut DynamicState,
) -> Vec<Arc<dyn FramebufferAbstract + Send + Sync>> {
    let dimensions = images[0].dimensions();

    let viewport = Viewport {
        origin: [0.0, 0.0],
        dimensions: [dimensions[0] as f32, dimensions[1] as f32],
        depth_range: 0.0..1.0,
    };

    dynamic_state.viewports = Some(vec![viewport]);
    images
        .iter()
        .map(|image| {
            Arc::new(
                Framebuffer::start(render_pass.clone())
                    .add(image.clone())
                    .unwrap()
                    .build()
                    .unwrap(),
            ) as Arc<_>
        })
        .collect::<Vec<_>>()
}
