use anyhow::Result;
use legion::*;

use crate::Time;

const WINDOW_SIZE: (u32, u32) = (800, 600);

pub struct Game {
    world: World,
    resources: Resources,
    schedule: Schedule,
}

impl Game {
    pub fn new(world: World, resources: Resources, schedule: Schedule) -> Self {
        Self {
            world,
            resources,
            schedule,
        }
    }

    pub fn poll(&mut self) {}

    pub fn proc(&mut self) {
        self.schedule.execute(&mut self.world, &mut self.resources);
        let mut time = self.resources.get_mut::<Time>().unwrap();
        time.start_frame();
    }

    pub fn render(&self) {}
}
